<!DOCTYPE html>
<html lang="en-us">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Source Themes Academic 4.7.0">

  

  
  
  
  
  
    
    
    
  
  

  <meta name="author" content="Guilherme Ferraz de Arruda">

  
  
  
    
  
  <meta name="description" content="Complex networks have become the main paradigm for modelling the dynamics of interacting systems. However, networks are intrinsically limited to describing pairwise interactions, whereas real-world systems are often characterized by higher-order interactions involving groups of three or more units. Higher-order structures, such as hypergraphs and simplicial complexes, are therefore a better tool to map the real organization of many social, biological and man-made systems. Here, we highlight recent evidence of collective behaviours induced by higher-order interactions, and we outline three key challenges for the physics of higher-order systems.">

  
  <link rel="alternate" hreflang="en-us" href="https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/">

  


  
  
  
  <meta name="theme-color" content="#2962ff">
  

  
  
  
  <script src="/js/mathjax-config.js"></script>
  

  
  
  
  
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/academicons/1.8.6/css/academicons.min.css" integrity="sha256-uFVgMKfistnJAfoCUQigIl+JfUaP47GrRKjf6CTPVmw=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" integrity="sha256-Vzbj7sDDS/woiFS3uNKo8eIuni59rjyNGtXfstRzStA=" crossorigin="anonymous">

    
    
    
      
    
    
      
      
        
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/styles/github.min.css" crossorigin="anonymous" title="hl-light">
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/styles/dracula.min.css" crossorigin="anonymous" title="hl-dark" disabled>
        
      
    

    

    

    
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.1.2/lazysizes.min.js" integrity="sha256-Md1qLToewPeKjfAHU1zyPwOutccPAm5tahnaw7Osw0A=" crossorigin="anonymous" async></script>
      
    
      

      
      

      
    
      

      
      

      
    
      

      
      

      
        <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js" integrity="" crossorigin="anonymous" async></script>
      
    
      

      
      

      
    

  

  
  
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CRoboto:400,400italic,700%7CRoboto+Mono&display=swap">
  

  
  
  
  
  <link rel="stylesheet" href="/css/academic.css">

  





<script async src="https://www.googletagmanager.com/gtag/js?id=UA-53460777-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
      dataLayer.push(arguments);
  }

  function trackOutboundLink(url) {
    gtag('event', 'click', {
         'event_category': 'outbound',
         'event_label': url,
         'transport_type': 'beacon',
         'event_callback': function () {
           document.location = url;
         }
    });
    console.debug("Outbound link clicked: " + url);
  }

  function onClickCallback(event) {
    if ((event.target.tagName !== 'A') || (event.target.host === window.location.host)) {
      return;
    }
    trackOutboundLink(event.target);  
  }

  gtag('js', new Date());
  gtag('config', 'UA-53460777-3', {});

  
  document.addEventListener('click', onClickCallback, false);
</script>


  


  

  <link rel="manifest" href="/index.webmanifest">
  <link rel="icon" type="image/png" href="/images/icon_hu0b7a4cb9992c9ac0e91bd28ffd38dd00_9727_32x32_fill_lanczos_center_2.png">
  <link rel="apple-touch-icon" type="image/png" href="/images/icon_hu0b7a4cb9992c9ac0e91bd28ffd38dd00_9727_192x192_fill_lanczos_center_2.png">

  <link rel="canonical" href="https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/">

  
  
  
  
  
    
    
  
  
  <meta property="twitter:card" content="summary">
  
  <meta property="twitter:site" content="@GuiFdeArruda">
  <meta property="twitter:creator" content="@GuiFdeArruda">
  
  <meta property="og:site_name" content="Guilherme Ferraz de Arruda">
  <meta property="og:url" content="https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/">
  <meta property="og:title" content="The physics of higher-order interactions in complex systems | Guilherme Ferraz de Arruda">
  <meta property="og:description" content="Complex networks have become the main paradigm for modelling the dynamics of interacting systems. However, networks are intrinsically limited to describing pairwise interactions, whereas real-world systems are often characterized by higher-order interactions involving groups of three or more units. Higher-order structures, such as hypergraphs and simplicial complexes, are therefore a better tool to map the real organization of many social, biological and man-made systems. Here, we highlight recent evidence of collective behaviours induced by higher-order interactions, and we outline three key challenges for the physics of higher-order systems."><meta property="og:image" content="img/map[gravatar:%!s(bool=false) shape:circle]">
  <meta property="twitter:image" content="img/map[gravatar:%!s(bool=false) shape:circle]"><meta property="og:locale" content="en-us">
  
    
      <meta property="article:published_time" content="2021-10-01T00:00:00&#43;00:00">
    
    <meta property="article:modified_time" content="2021-10-01T00:00:00&#43;00:00">
  

  


    











<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Article",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/"
  },
  "headline": "The physics of higher-order interactions in complex systems",
  
  "datePublished": "2021-10-01T00:00:00Z",
  "dateModified": "2021-10-01T00:00:00Z",
  
  "author": {
    "@type": "Person",
    "name": "F. Battiston"
  },
  
  "publisher": {
    "@type": "Organization",
    "name": "Guilherme Ferraz de Arruda",
    "logo": {
      "@type": "ImageObject",
      "url": "img/https://guifarruda.gitlab.io/"
    }
  },
  "description": "Complex networks have become the main paradigm for modelling the dynamics of interacting systems. However, networks are intrinsically limited to describing pairwise interactions, whereas real-world systems are often characterized by higher-order interactions involving groups of three or more units. Higher-order structures, such as hypergraphs and simplicial complexes, are therefore a better tool to map the real organization of many social, biological and man-made systems. Here, we highlight recent evidence of collective behaviours induced by higher-order interactions, and we outline three key challenges for the physics of higher-order systems."
}
</script>

  

  


  


  





  <title>The physics of higher-order interactions in complex systems | Guilherme Ferraz de Arruda</title>

</head>

<body id="top" data-spy="scroll" data-offset="70" data-target="#TableOfContents" >

  <aside class="search-results" id="search">
  <div class="container">
    <section class="search-header">

      <div class="row no-gutters justify-content-between mb-3">
        <div class="col-6">
          <h1>Search</h1>
        </div>
        <div class="col-6 col-search-close">
          <a class="js-search" href="#"><i class="fas fa-times-circle text-muted" aria-hidden="true"></i></a>
        </div>
      </div>

      <div id="search-box">
        
        <input name="q" id="search-query" placeholder="Search..." autocapitalize="off"
        autocomplete="off" autocorrect="off" spellcheck="false" type="search">
        
      </div>

    </section>
    <section class="section-search-results">

      <div id="search-hits">
        
      </div>

    </section>
  </div>
</aside>


  







<nav class="navbar navbar-expand-lg navbar-light compensate-for-scrollbar" id="navbar-main">
  <div class="container">

    
    <div class="d-none d-lg-inline-flex">
      <a class="navbar-brand" href="/">Guilherme Ferraz de Arruda</a>
    </div>
    

    
    <button type="button" class="navbar-toggler" data-toggle="collapse"
            data-target="#navbar-content" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
    <span><i class="fas fa-bars"></i></span>
    </button>
    

    
    <div class="navbar-brand-mobile-wrapper d-inline-flex d-lg-none">
      <a class="navbar-brand" href="/">Guilherme Ferraz de Arruda</a>
    </div>
    

    
    
    <div class="navbar-collapse main-menu-item collapse justify-content-end" id="navbar-content">

      
      <ul class="navbar-nav d-md-inline-flex">
        

        

        
        
        
          
        

        
        
        
        
        
        
          
          
          
            
          
          
        

        <li class="nav-item">
          <a class="nav-link " href="/#about"><span>Home</span></a>
        </li>

        
        

        

        
        
        
          
        

        
        
        
        
        
        
          
          
          
            
          
          
        

        <li class="nav-item">
          <a class="nav-link " href="/#featured"><span>Featured Research</span></a>
        </li>

        
        

        

        
        
        
          
        

        
        
        
        
        
        
          
          
          
            
          
          
        

        <li class="nav-item">
          <a class="nav-link " href="/#publications"><span>Publications</span></a>
        </li>

        
        

        

        
        
        
          
        

        
        
        
        
        
        

        <li class="nav-item">
          <a class="nav-link " href="/courses/"><span>Courses</span></a>
        </li>

        
        

        

        
        
        
          
        

        
        
        
        
        
        
          
          
          
            
          
          
        

        <li class="nav-item">
          <a class="nav-link " href="/#contact"><span>Contact</span></a>
        </li>

        
        

        

        
        
        
          
            
          
        

        
        
        
        
        
        

        <li class="nav-item">
          <a class="nav-link " href="https://gitlab.com/guifarruda/guifarruda.gitlab.io/raw/master/public/cv/cv.pdf" target="_blank" rel="noopener"><span>CV</span></a>
        </li>

        
        

        

        
        
        
          
        

        
        
        
        
        
        

        <li class="nav-item">
          <a class="nav-link " href="/aboutme/"><span>About Me</span></a>
        </li>

        
        

      

        
      </ul>
    </div>

    <ul class="nav-icons navbar-nav flex-row ml-auto d-flex pl-md-2">
      
      <li class="nav-item">
        <a class="nav-link js-search" href="#"><i class="fas fa-search" aria-hidden="true"></i></a>
      </li>
      

      
      <li class="nav-item">
        <a class="nav-link js-dark-toggle" href="#"><i class="fas fa-moon" aria-hidden="true"></i></a>
      </li>
      

      

    </ul>

  </div>
</nav>


  <script type='text/javascript' src='https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js'></script>


<script type="text/javascript" src="//cdn.plu.mx/widget-details.js"></script>

<div class="pub">

  












  

  
  
  
<div class="article-container pt-3">
  <h1>The physics of higher-order interactions in complex systems</h1>

  

  
    


<div class="article-metadata">

  
  
  
  
  <div>
    



  
  <span>F. Battiston</span>, <span>E. Amico</span>, <span>A. Barrat</span>, <span>G. Bianconi</span>, <span>G. F. de Arruda</span>, <span>B. Franceschiello</span>, <span>I. Iacopini</span>, <span>S. Kefi</span>, <span>V. Latora</span>, <span>Y Moreno</span>, <span>M. M. Murray</span>, <span>T. P. Peixoto</span>, <span>F. Vaccarino</span>, <span>G. Petri</span>

  </div>
  
  

  
  <span class="article-date">
    
    
      
    
    October 2021
  </span>
  

  

  

  
  
  

  
  

</div>

    











  



<div class="btn-links mb-3">
  
  








  
    
  



<a class="btn btn-outline-primary my-1 mr-1" href="https://www.nature.com/articles/s41567-021-01371-4" target="_blank" rel="noopener">
  PDF
</a>



<button type="button" class="btn btn-outline-primary my-1 mr-1 js-cite-modal"
        data-filename="/publication/thephysicsofhigher-orderinteractionsincomplexsystems/cite.bib">
  Cite
</button>













<a class="btn btn-outline-primary my-1 mr-1" href="https://doi.org/10.1038/s41567-021-01371-4" target="_blank" rel="noopener">
  DOI
</a>


  
  
  
    
  
  
  
  
  
    
  
  <a class="btn btn-outline-primary my-1 mr-1" href="http://arxiv.org/abs/2110.06023" target="_blank" rel="noopener">
    
    arXiv
  </a>


</div>


  
</div>



  <div class="article-container">

    
    <h3>Abstract</h3>
    <p class="pub-abstract">Complex networks have become the main paradigm for modelling the dynamics of interacting systems. However, networks are intrinsically limited to describing pairwise interactions, whereas real-world systems are often characterized by higher-order interactions involving groups of three or more units. Higher-order structures, such as hypergraphs and simplicial complexes, are therefore a better tool to map the real organization of many social, biological and man-made systems. Here, we highlight recent evidence of collective behaviours induced by higher-order interactions, and we outline three key challenges for the physics of higher-order systems.</p>
    

    
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="row">
          <div class="col-12 col-md-3 pub-row-heading">Type</div>
          <div class="col-12 col-md-9">
            
            
            <a href="/publication/#2">
              Journal article
            </a>
            
          </div>
        </div>
      </div>
      <div class="col-md-1"></div>
    </div>
    <div class="d-md-none space-below"></div>
    

    
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="row">
          <div class="col-12 col-md-3 pub-row-heading">Publication</div>
          <div class="col-12 col-md-9">Nature Physics</div>
        </div>
      </div>
      <div class="col-md-1"></div>
    </div>
    <div class="d-md-none space-below"></div>
    

    <div class="space-below"></div>

    <div class="article-style"></div>

    <a data-badge-details="right" data-badge-type="medium-donut" data-hide-no-mentions="true" class="altmetric-embed" data-doi="10.1038/s41567-021-01371-4"></a>

    <div class="space-below"></div>

    <a href="https://plu.mx/plum/a/?doi=10.1038%2fs41567-021-01371-4" data-hide-print="true" data-border="false" class="plumx-details" data-site="plum" data-hide-when-empty="true"></a>

    





<div class="share-box" aria-hidden="true">
  <ul class="share">
    
      
      
      
        
      
      
      
      <li>
        <a href="https://twitter.com/intent/tweet?url=https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/&amp;text=The%20physics%20of%20higher-order%20interactions%20in%20complex%20systems" target="_blank" rel="noopener" class="share-btn-twitter">
          <i class="fab fa-twitter"></i>
        </a>
      </li>
    
      
      
      
        
      
      
      
      <li>
        <a href="https://www.facebook.com/sharer.php?u=https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/&amp;t=The%20physics%20of%20higher-order%20interactions%20in%20complex%20systems" target="_blank" rel="noopener" class="share-btn-facebook">
          <i class="fab fa-facebook"></i>
        </a>
      </li>
    
      
      
      
        
      
      
      
      <li>
        <a href="mailto:?subject=The%20physics%20of%20higher-order%20interactions%20in%20complex%20systems&amp;body=https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/" target="_blank" rel="noopener" class="share-btn-email">
          <i class="fas fa-envelope"></i>
        </a>
      </li>
    
      
      
      
        
      
      
      
      <li>
        <a href="https://www.linkedin.com/shareArticle?url=https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/&amp;title=The%20physics%20of%20higher-order%20interactions%20in%20complex%20systems" target="_blank" rel="noopener" class="share-btn-linkedin">
          <i class="fab fa-linkedin-in"></i>
        </a>
      </li>
    
      
      
      
        
      
      
      
      <li>
        <a href="https://web.whatsapp.com/send?text=The%20physics%20of%20higher-order%20interactions%20in%20complex%20systems%20https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/" target="_blank" rel="noopener" class="share-btn-whatsapp">
          <i class="fab fa-whatsapp"></i>
        </a>
      </li>
    
      
      
      
        
      
      
      
      <li>
        <a href="https://service.weibo.com/share/share.php?url=https://guifarruda.gitlab.io/publication/thephysicsofhigher-orderinteractionsincomplexsystems/&amp;title=The%20physics%20of%20higher-order%20interactions%20in%20complex%20systems" target="_blank" rel="noopener" class="share-btn-weibo">
          <i class="fab fa-weibo"></i>
        </a>
      </li>
    
  </ul>
</div>












  
  
    
  
  














  
  



  </div>
</div>

      

    
    
    
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.min.js" integrity="sha256-lqvxZrPLtfffUl2G/e7szqSvPBILGbwmsGE1MKlOi0Q=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js" integrity="sha256-CBrpuqrMhXwcLLUd5tvQ4euBHCdh7wGlDfNz8vbu/iI=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha256-yt2kYMy0w8AbtF89WXb2P1rfjcP/HTHLT7097U8Y5b8=" crossorigin="anonymous"></script>

      

      
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/highlight.min.js" integrity="sha256-1zu+3BnLYV9LdiY85uXMzii3bdrkelyp37e0ZyTAQh0=" crossorigin="anonymous"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/languages/r.min.js"></script>
        
      

    

    
    

    
    
    <script>const code_highlighting = true;</script>
    

    
    
    
    
    
    
    <script>
      const search_config = {"indexURI":"/index.json","minLength":1,"threshold":0.3};
      const i18n = {"no_results":"No results found","placeholder":"Search...","results":"results found"};
      const content_type = {
        'post': "Posts",
        'project': "Projects",
        'publication' : "Publications",
        'talk' : "Talks"
        };
    </script>
    

    
    

    
    
    <script id="search-hit-fuse-template" type="text/x-template">
      <div class="search-hit" id="summary-{{key}}">
      <div class="search-hit-content">
        <div class="search-hit-name">
          <a href="{{relpermalink}}">{{title}}</a>
          <div class="article-metadata search-hit-type">{{type}}</div>
          <p class="search-hit-description">{{snippet}}</p>
        </div>
      </div>
      </div>
    </script>
    

    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fuse.js/3.2.1/fuse.min.js" integrity="sha256-VzgmKYmhsGNNN4Ph1kMW+BjoYJM2jV5i4IlFoeZA9XI=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.min.js" integrity="sha256-4HLtjeVgH0eIB3aZ9mLYF6E8oU5chNdjU6p6rrXpl9U=" crossorigin="anonymous"></script>
    

    
    

    
    

    
    
    
    
    
    
    
    
    
      
    
    
    
    
    <script src="/js/academic.min.0630fec5958cb075a5a38f042b3ddde6.js"></script>

    






  
  
  <div class="container">
    <footer class="site-footer">
  

  <p class="powered-by">
    Guilherme Ferraz de Arruda 2024 &middot; 

    Powered by the
    <a href="https://sourcethemes.com/academic/" target="_blank" rel="noopener">Academic theme</a> for
    <a href="https://gohugo.io" target="_blank" rel="noopener">Hugo</a>.

    
    <span class="float-right" aria-hidden="true">
      <a href="#" class="back-to-top">
        <span class="button_icon">
          <i class="fas fa-chevron-up fa-2x"></i>
        </span>
      </a>
    </span>
    
  </p>
</footer>

  </div>
  

  
<div id="modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Cite</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <pre><code class="tex hljs"></code></pre>
      </div>
      <div class="modal-footer">
        <a class="btn btn-outline-primary my-1 js-copy-cite" href="#" target="_blank">
          <i class="fas fa-copy"></i> Copy
        </a>
        <a class="btn btn-outline-primary my-1 js-download-cite" href="#" target="_blank">
          <i class="fas fa-download"></i> Download
        </a>
        <div id="modal-error"></div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
